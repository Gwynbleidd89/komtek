﻿using System.Runtime.InteropServices;

namespace Managed_Wrappers.Managed
{
    public static class CalcWrapper
    {
        [DllImport(@"Unmanaged\Calculator.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Solve(string expression, out double result);

        [DllImport(@"Unmanaged\Calculator.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern string GetLastError();
    }
}