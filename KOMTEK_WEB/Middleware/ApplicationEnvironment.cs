﻿using Microsoft.AspNetCore.Http;

namespace KOMTEK_WEB.Middleware
{
  
    internal class AppEnvironment
    {
        private static HttpContext _httpContext;
        private static IHttpContextAccessor _httpContextAccessor;

        private static HttpContext _current => _httpContextAccessor.HttpContext;

        public static string BaseUrl =>
            $"{_current.Request.Scheme}://{_current.Request.Host}{_current.Request.PathBase}";

        internal static void Configure(IHttpContextAccessor contextAccessor)
        {
            _httpContextAccessor = contextAccessor;
        }
    }
}