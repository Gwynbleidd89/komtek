﻿using System.ComponentModel.DataAnnotations;

namespace KOMTEK_WEB.ViewModels
{
    public class SolveVM
    {
        [MinLength(1)]
        public string Expression { get; set; }

        public string Result { get; set; }
    }
}