﻿using Managed_Wrappers.Managed;
using Microsoft.AspNetCore.Mvc;


namespace KOMTEK_WEB.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class CalculatorController : ControllerBase
    {

        [HttpGet]
        public ActionResult<string> Get(string expression)
        {
            if (string.IsNullOrWhiteSpace(expression))
            {
                return BadRequest();
            }

            try
            {
                return CalcWrapper.Solve(expression, out double result)
                    ? result.ToString()
                    : CalcWrapper.GetLastError();
            }
            catch
            {
                return "The expression is not recognized.";
            }
        }
    }
}