﻿using KOMTEK_WEB.Middleware;
using KOMTEK_WEB.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace KOMTEK_WEB.Controllers
{
    public class SolverController : Controller
    {
        private readonly IHttpClientFactory _clientFactory;

        private static string HttpRoute = "";
        private readonly string Controller = nameof(API.CalculatorController)
            .Replace("Controller", string.Empty);

        private Uri BaseAdress => new Uri(AppEnvironment.BaseUrl);

        public SolverController(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        [HttpGet]
        public async Task<ActionResult<SolveVM>> Solve(string expression)
        {
            if (string.IsNullOrWhiteSpace(expression))
            {
                return View();
            }

            var solve = new SolveVM
            {
                Expression = expression
            };

            HttpClient client = _clientFactory.CreateClient();
            client.BaseAddress = BaseAdress;
            string url = Url.RouteUrl("DefaultApi", new { HttpRoute, Controller, expression });
            HttpResponseMessage response = await client.GetAsync(url);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                return StatusCode((int)response.StatusCode);
            }

            solve.Result = await response.Content.ReadAsStringAsync();

            return View(solve);
        }
    }
}