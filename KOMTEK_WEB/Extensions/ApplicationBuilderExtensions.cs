﻿using KOMTEK_WEB.Middleware;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace KOMTEK_WEB.Extensions
{
    internal static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseAppEnvironment(this IApplicationBuilder applicationBuilder)
        {
            AppEnvironment.Configure(
                applicationBuilder.ApplicationServices.GetRequiredService<IHttpContextAccessor>());

            return applicationBuilder;
        }
    }
}