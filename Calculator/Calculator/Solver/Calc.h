#include <iostream>
#include <fstream>
#include <math.h>
#include <ctype.h>
#include "St_q.h"
#include "var.h"

class Calc {
private:
	int _errorCode;
	stack_ch st_ch;
	stack_d st_d;
	q_type que;
	stack_var st_var;
	char stroka[100];
	char str[20];
	
	int i;
	int counter;
	int separator;
	
	bool Worker1();
	bool Worker2();
	void Worker3();
	
	void Reset(char* inputString);
	bool MakeOperation(char sym);

public:
	bool Solve(char* string, double& ans);
	char* GetLastError();
};
