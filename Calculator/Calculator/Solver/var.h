#pragma once
#include <map>
#include <cstring>
#include <iostream>
#include <string>
using namespace std;

// ���������� �� ����� ���� ������ 20 ��������
// ��...��� ��� ����������!
#define size 20

///-------���_����������-------///
class name {
	// ��� ����������
	string str;
	// �����-�� ������
	int index;
	
	public:
	// �����������
	name (string);
	// ���������� ��������� �� ������ "��� ����������"
	// �� �����, ������ �����, ���������� ��� ����������
	string get();
};

// ��������� map ������� �� ���� ���������:
// ��� ���������� � �������� ����������.
// �� ����� ���������� (�.�. �� ����� � ����� ������)
// �� ������ ����� �������� ����������.
// ����� ���� ����������� ������������� �� ��������� map,
// ��������� ���� ��������� �������� ���������
bool operator< (name a, name b);


class variable {
	// �������� ����������
	double var;
public:
	// �����������
	variable(double);
	// ���������� �������� ���������� 
	double get_var();
	// ������������� �������� ����������
	void set_var(double);
};

///--- ��������, � ���� �� � map ����� ���������� ---///
bool search (string VAR_NAME);

// ���� ���������� � ����� ������ ��-���� ����, ��
///--- �� ����� ���������� ������������ � �������� ---///
double find_return (string VAR_NAME);

// ���������� ���������, ������ ���������� ����� ����������
// ������ � ��� ������, ���� ���� �������������������
void new_var (string VAR_NAME, double);

// ���������� �������� ����������, ����� ���� �������� ������������
void def_var (string VAR_NAME, double);

// ��� ��� ���������� (��� ������ ����������)
// ���� ����� ���� (��� ���������� �������������� ������������)
class stack_var {
	string stck[size];
	int tos;
public:
	stack_var() { tos = 0; }
	void push(string);
	string pop();
	string top();
};

