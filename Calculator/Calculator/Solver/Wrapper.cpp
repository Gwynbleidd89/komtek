#include "Calc.h"

static Calc calc;

extern "C" __declspec(dllexport) bool __cdecl Solve(char* expression, double& result)
{
	return calc.Solve(expression, result);
}

extern "C" __declspec(dllexport) char* __cdecl GetLastError()
{
	return calc.GetLastError();
}