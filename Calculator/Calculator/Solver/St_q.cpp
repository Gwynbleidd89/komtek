#include "St_q.h"

///------- ������ ��������� ��� ������� -------///
// ����������� ��� ��������� � ���� ��� ������� �����
BCD:: BCD (double num)
{
	token.d = num;
	flag = 1;
}

// ����������� ��� ��������� ������� (����� ��������)
BCD:: BCD (char symb)
{
	token.ch = symb;
	flag = 2;
}

// ����������� ��� ������ (������ - ��� ����������)
BCD:: BCD (char* s)
{
	strcpy(token.name, s);
	flag = 0;
}

// ����������� ��� �������������� ��������: ������� �����
BCD:: BCD (bool b)
{
	token.b = true;
}

// �����
ostream& operator<<(ostream& stream, const BCD& c)
{
	if(c.flag == 1) return stream << c.token.d;
	if(c.flag == 2) return stream << c.token.ch;
	if(c.flag == 0) return stream << c.token.name;
	if(c.token.b == true) return stream << "STOP";
}

// �������� ���������, 
bool BCD:: operator==(char ch)
{
	if(token.ch == ch) return true;
	else return false;
}

// �������� ���������
bool BCD:: operator!=(char ch)
{
	if(token.ch != ch) return true;
	else return false;
}

///------- ������ ��������� ��� ����� ����� -------///
// ����������� ��� ��������. ��������: ���� ����
DB:: DB(bool b)
{
	token.b = true;
	flag = true;
}

// ����������� ��� �����
DB:: DB(double number)
{
	token.d = number;
	flag = false;
}

///------- ������ �� ������ ��� ����� -------///
// ����������� � ����
void stack_d:: push(DB ob)
{
	if (tos == SIZE)
	{
		return;
	}
	
	stck[tos] = ob;
	tos++;
}

// ���������� �������� �� �����
DB stack_d::pop()
{
	if (tos == 0)
	{
		DB ob(true);
		return ob;
	}
	
	tos--;
	return stck[tos];
}

// �������� �������� ��� ����������
DB stack_d:: top()
{
	if (tos == 0)
	{
		DB ob(true);
		return ob;
	}
	
	tos--;
	DB temp = stck[tos];
	tos++;
	return temp;
}

///------- ������ �� ������ ��� ��������(����������) -------///
// ����������� � ����
void stack_ch:: push(char ob)
{
	if (tos == SIZE)
	{
		return;
	}
	
	stck[tos] = ob;
	tos++;
}

// ���������� �������� �� �����
char stack_ch:: pop()
{
	if (tos == 0)
	{
		return '0';
	}
	
	tos--;
	return stck[tos];
}

// �������� �������� ��� ����������
char stack_ch:: top()
{
	if (tos == 0)
	{
		return '0';
	}
	
	tos--;
	char temp = stck[tos];
	tos++;
	return temp;
}

///-------������� � ���� �������-------///

// ������� � �������
void q_type:: q(BCD ob)
{
	if ( (tail+1 == head) || (tail+1 == SIZE && !head) )
	{
		return;
	}
	
	tail++;
	if (tail == SIZE) tail = 0; 
	queue[tail] = ob;
}

// ���������� �������� �� �������
BCD q_type:: deq()
{
	if (head == tail)
	{
		BCD ob(true);
		return ob;
	}
	
	head++;
	if (head == SIZE) head = 0;
    return queue[head];
}

// �������� �������� ��� ����������
BCD q_type:: eq()
{
	if (head == tail)
	{
		BCD ob(true);
		return ob;
	}
	if (head == SIZE) head = 0;
	head++;
	BCD temp = queue[head];
	head--;
	return temp;
}

/*int main () {
q_type x;
char sym = '+';
x.q(sym);
cout << x.deq() << endl;
return 0;
}*/

