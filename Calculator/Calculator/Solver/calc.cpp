#include "Calc.h";


int get_oper(char sym)
{
	switch(sym)
	{
		case '=': {return 1; break;}
		case '(': {return -1; break;} 
		case ')': {return 0; break;}
		case '+': {return 2; break;}
		case '-': {return 2; break;}
		case '*': {return 3; break;}
		case '/': {return 3; break;}
		case '^': {return 4; break;}
		case '!': {return 5; break;}
		default: return -10;
	}
}

char* string_(char* input)
{
	int i = 0;
	int j = 0;
	char* str = new char[strlen(input)+1];
	str[strlen(input)] = '\0';
	while(input)
	{
		if (input[i] == '/' && input[i+1] == '/')
		{
			break;
		}
		
		if (input[i] == '/' && input[i+1] == '*')
		{
			i = i+2;
			while(input[i] != '*' && input[i+1] != '/') i++;
			i = i+2;
		}
		
		if (input[i] == ' ' || input[i] == '	');
		else
		    str[j++] = input[i];
		if (input[i] == '\0') break;
		i++;
	}
	strcpy(input, str);
	delete[] str;
	return input;
}

void Calc:: Reset(char* arg)
{
	BCD x;
	DB y;
	char z;
	
	_errorCode = 0;
	
	while(1)
	{
		y = st_d.pop();
		if (y.token.b == true) break;
	}
	
	while(1)
	{
		z = st_ch.pop();
		if (z == '0') break;
	}
	
	while(1)
	{
		x = que.deq();
		if (x.token.b == true) break;
	}
	
	while(st_var.pop() != "1")
	
	counter = 0;
	separator = 0;
	
	strcpy_s(stroka, arg);
	stroka[strlen(stroka)] = '\0';
}

bool Calc:: Worker1()
{
	char x = st_ch.top();
	while(x != '(' && x != '0')
	{
		que.q(st_ch.pop());
		x = st_ch.top();
	}
	
	if(x == '0')
	{ 
		_errorCode = 1;
		return true;
	}
	
	st_ch.pop();
	return false;
}

bool Calc:: Worker2()
{
	bool flag;
	
	counter = 0;
	
	strcpy_s(stroka, string_(stroka));
		
	for (i = separator; stroka[i]; i++)
	{
		char str[20];
		int index = 0;
		flag = false;
		
		int count_point = 0;
		while(isdigit(stroka[i]) || stroka[i] == '.')
		{
			if(stroka[i] == '.')
			{
				count_point++;
				if(count_point > 1)
				{
					_errorCode = 4;
					return true;
				}
			}
			i++;
			flag = true;
		}
		int count_len = 0;
		if(isalpha(stroka[i]))
		{
			while(isdigit(stroka[i]) || isalpha(stroka[i]) || stroka[i]=='_')
			{
				str[index++] = stroka[i++];
				count_len++;
				flag = true;
			}
			if(count_len > 19)
			{
				_errorCode = 7;
				return true;
			}
			str[index] = '\0';
			
			if(!search(str) && stroka[i] != '=')
			{
				flag = false;
				_errorCode = 2;
				return true;
			}
			else
			{
				if(!search(str)) { new_var(str, 0); }
			}
		}
		
		if(flag) {counter++; i--;}
		
		if(stroka[i]=='+' || stroka[i]=='-' || stroka[i]=='*'
		|| stroka[i]=='/' || stroka[i]=='^' || stroka[i]=='=')
		{
			if(counter == 0)
			{
				if(stroka[i]=='+') {stroka[i]=' ';}
				else
				{
					if(stroka[i]=='-')
					{
						stroka[i]='!';
					}
					else
					{ counter--;}
				}
			}
			else
			{
				counter--;
			}
		}
		else 
		if(!(isalpha(stroka[i]) || isdigit(stroka[i]) || stroka[i] == '.')
			&& stroka[i] != '(' && stroka[i] != ')' && stroka[i] != ':')
		{
			_errorCode = 8;
			return true;		
		}

		if(stroka[i] == ':')
		{
			if(counter != 1)
			{ 
				_errorCode = 3;
				return true;
			}
			return false;
		}
	}
	if(counter != 1)
	{ 
		_errorCode = 3;
		return true;
	}
	return false;
}

void Calc:: Worker3()
{
	BCD x = que.eq();
	int index = strlen(x.token.name) - 1;
	if(x.token.name[index] == '~')
	{
		x.token.name[index] = '\0';
		st_var.push(x.token.name);
	}
	else 
	{
		st_d.push(find_return(x.token.name));
	}
}

bool Calc:: MakeOperation(char sym)
{
	double a, b;
	DB x = st_d.pop();
	
	if(x.flag == false) { a = x.token.d; }
	if(x.flag == false && sym != '!' && sym != '=')
	{
		x = st_d.pop();
		b = x.token.d;
	}
	switch(sym)
	{
		case '+':
				{
					st_d.push(a + b);
					break;
				}
		case '-':
				{
					st_d.push((a-b)*(-1));
					break;
				}
		case '*':
				{
					st_d.push(a * b);
					break;
				}
		case '/':
				{
					st_d.push(b/a);
					break;
				}
		case '!':
				{
					st_d.push(-a);
					break;
				}
		case '^':
				{
					st_d.push(pow(b,a));
					break;
				}
		case '=':
				{
					if(st_var.top() == "1")
					{	
						_errorCode = 6;
						return false;
					}
					else
					{
						def_var(st_var.top(), a);
						st_d.push(find_return(st_var.pop()));
						break;
					}
				}
		default: ;
	}
	return true;
}

bool  Calc:: Solve(char* input, double& ans)
{
	Reset(input);
	BCD x;
	ofstream out ("log.txt", ios:: out | ios :: app);
	if (!out)
	{
		return false;
	}
	out << stroka << " is ";
	while(1)
	{
		char token[100];
		int index = 0;
		
		bool flag;
		
		if(Worker2())
			 break;
		
		for(i = separator; stroka[i] != '\0'; i++)
		{
			flag = false;
			
			if(stroka[i]=='(' || stroka[i]==')' || stroka[i]=='+' 
			|| stroka[i]=='-' || stroka[i]=='*' || stroka[i]=='/' 
			|| stroka[i]=='^' || stroka[i]=='!' || stroka[i]=='=')
			{
				if(stroka[i]==')')
				{
					if(Worker1())
					{
						break;
					}
				}
				else
				{
					if(stroka[i]=='(')
					{
						st_ch.push(stroka[i]);
					}
					else if(st_ch.top() != '0')
					{
						if (get_oper(stroka[i]) <= get_oper(st_ch.top()))
						{
							while(get_oper(stroka[i]) <= get_oper(st_ch.top()))
							{
								que.q(st_ch.pop());
							}
						}
						st_ch.push(stroka[i]);
					}
					if(st_ch.top() == '0') st_ch.push(stroka[i]);
				}
			}
			else
			{
				
				if (isdigit(stroka[i]) || stroka[i]=='.')
				{
					while(isdigit(stroka[i]) || stroka[i]=='.')
					{
						token[index++] = stroka[i++];
					}
					token[index] = 0;
					index = 0;
					que.q(atof(token));
					flag = true;
				}
				
				bool fl = true;
				if (isalpha(stroka[i]))
				{
					while(isalpha(stroka[i]) || isdigit(stroka[i])
					|| stroka[i]=='_')
					{
						token[index++] = stroka[i++];
						if(stroka[i] == '=')
						{
							token[index] = '~';
							token[index + 1] = '\0';
							fl = false;
							break;
						}
					}
					if(fl == true) token[index] = '\0';
					index = 0;
					que.q(token);
					flag = true;
				}
				
				if(flag) i--;
				
				if(stroka[i] == ':')
				{
					separator = i + 1;
					if(stroka[separator])
					{
						counter = 0;
						break;
					}
				}
			}
		}
		
		while(st_ch.top() != '0')
		{
			if(st_ch.top() == '(')
			{
				_errorCode = 5;
				break;
			}
			que.q(st_ch.pop());
		}
		
		x = que.eq();
		bool xflag = false;
		while(1)
		{
			if(x.flag == 1)
			{
				st_d.push(x.token.d);
				que.deq();
			}
			if(x.flag == 0)
			{
				Worker3();
				que.deq();
			}
			if ( x.flag == 2)
			{
				if(!MakeOperation(x.token.ch))
				{
					xflag = true;
					break;
				}
				que.deq();
			}
			x = que.eq();
			if(x.token.b == true) break;
		}
		
		if (xflag)
			break;
		
		DB y = st_d.pop();
		ans = y.token.d;
		out << ans <<';';
		
		if(counter != 0) {
			break;
		}
		out << ' ';
	}
	if (_errorCode > 0)
	{
		out << "ERROR # " << _errorCode << endl;
		out.close();
		return false;
	}
	else
		out << endl;
	out.close();
	return true;
}

char* Calc:: GetLastError()
{
	switch(_errorCode)
	{
	    case 1:	return "Error_01: missing token '('";
	    case 2:	return "Error_02: identifier not found";
		case 3:	return "Error_03: missing operand";
		case 4:	return "Error_04: incorrect number";
		case 5:	return "Error_05: missing token ')'";
	    case 6:	return "Error_06: incorrect initialization";
		case 7:	return "Error_07: long name of variable";
		case 8:	return "Error_08: token does not belong to the grammar";
		default: return "no Error";
	}
}

//int main(int argc, char* argv[])
//{
//	Calc ob;
//	double ans;
//	if (ob.Solve("( (b=(a = 10)*(a-5)) - (150/b) -7 )^2", ans) == true)
//	{
//		cout << ans << endl;		
//	}
//	else
//	{
//		cout << ob.GetLastError() << endl;
//	}
//
//return 0;
//}
